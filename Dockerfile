FROM openjdk:8-alpine

COPY target/uberjar/ludus.jar /ludus/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/ludus/app.jar"]
