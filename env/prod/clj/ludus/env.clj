(ns ludus.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[ludus started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[ludus has shut down successfully]=-"))
   :middleware identity})
