(ns ludus.apps.auth
  (:require
   [ring.util.http-response :refer [content-type ok]]
   [ring.util.anti-forgery :refer [anti-forgery-field]]
   [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
   [ludus.db.core :as db]
   [ludus.layout.home :as layout]
   [compojure.core :refer [defroutes GET POST]]
   [ring.util.response :refer [response]]
   [clojure.java.io :as io])
  (:use
   [hiccup.core :only (html)]
   [hiccup.page :only (html5 include-css include-js)]
   [ring.util.anti-forgery]))

;;(defn login-Page [& content]
;;  (base "FSHM"))

(defn show-login-page []
;  (let [a (db/show-signatories!)]
  (-> (response (layout/base "sexy"))
      (assoc :headers {"Content-Type" "text/html"})))


(defroutes authentication-routes
  ;;(GET "/" [] (home-page false))
  ;;(GET "/ta" [] (home-page-ta false))
  ;;(GET "/support" [] (support-page))
  ;;(POST "/support" data (add-support (:params data)))
  (GET "/login" [] (show-login-page)))
