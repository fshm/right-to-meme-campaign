-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(id, first_name, last_name, email, pass)
VALUES (:id, :first_name, :last_name, :email, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET first_name = :first_name, last_name = :last_name, email = :email
WHERE id = :id

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id

-- :name show-support! :! :n
-- :doc creates a support record for the meity letter
INSERT INTO meity_letter
VALUES (:email, :name, :org, :subscribe)

-- :name count-support! :? :1
-- :doc count the support for the open letter
SELECT COUNT(DISTINCT(email)), COUNT(DISTINCT(org))
FROM meity_letter;


-- :name search-signatory! :? :1
-- :doc search the signatory for the open letter
SELECT email FROM meity_letter WHERE email = :email;

-- :name show-signatories! :? :m
-- :doc show all signatories for the open letter
SELECT DISTINCT ON(email) * FROM meity_letter;

